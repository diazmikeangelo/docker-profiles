module.exports = {
  lintOnSave: false,
  devServer: {
    public: process.env.PUBLIC,
  },
};
