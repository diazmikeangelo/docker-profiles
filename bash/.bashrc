export PS1="\u[\W]\n? "

alias art="php artisan $1"
alias phpunit="vendor/bin/phpunit $1"